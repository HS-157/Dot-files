#!/usr/bin/python3
import sys
import time
import shutil

columns = shutil.get_terminal_size()[0]
numberOfLineToShow = 0

if len(sys.argv) > 1:
    try:
        numberOfLineToShow = int(sys.argv[1])
    except:
        print("L'argument passé doit être un entier ")
        exit(1)

k = 0
lines = ""

# Read from pipe
while True:
    line = sys.stdin.readline()
    if not line:
        break
    lines += line

fname = "/tmp/logo"

num_chars = dict()
maxChar = 0

num_lines = 0

for line in lines.split('\n'):
    num_chars[num_lines] = len(line)
    num_lines += 1
    maxChar = max(maxChar,len(line))

with open(fname, 'w') as f:
    num_lines = 0
    maxChar = maxChar+5
    if maxChar*2 < columns: # par sur le fait qu'alsi a une sortie équivalente a celle de cowsay
        for line in lines.split('\n'):
            line += " "*(maxChar - num_chars[num_lines])
            f.write(line + "\n")
            num_lines += 1
    elif maxChar > columns:
        exit(1)
    else:
        maxChar = 1
    while num_lines < numberOfLineToShow:
        line = " " * (maxChar) + "\n"
        f.write(line)
        num_lines += 1
