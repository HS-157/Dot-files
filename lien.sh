#!/bin/sh

################################################
#                                              #
#                   lien.sh                    #
#              Écrit par HS-157                #
#              24 Novembre 2015                #
#              hs-157@mdl29.net                #
#                                              #
#   Créer les liens symboliques entre le git   #
#      et les fichiers de configuration        #
#                                              #
################################################

menu()							# Fonction pour afficher un menu
{								# Prend en entrée une liste
 	PS3="Choississez : "		#
	select choix				# Et renvoie qu'un seul élément de cette liste
	do							# Dans la variable $choix
	echo "Votre choix : $choix"	#
	break						#
	done						#
}

menu_multiple()																					# Fonction pour afficher un menu à choix multiple
{																								# Prend en entrée une liste
	file=("$@")																					# Et renvoie une liste des choix sélectionné
	menuitems()																					#
	{																							# Permet de sélectionner plusieurs item dans le menu
		echo "Option valabe :"																	# Et de la renvoyer sous la forme d'une liste
		for i in "${!file[@]}"; do																#
			printf "%3d%s) %s\n" $((i+1)) "${choices[i]:- }" "${file[i]}"						# Renvoie la liste des choix sélectionné dans la variable $tableau
		done																					#
		[[ "$msg" ]] && echo "$msg"; :															# Script originel : http://serverfault.com/a/298312
	}																							#
																								#
	prompt="Choississez (Appuyer sur Entrée pour valider) : "									#
	while menuitems && read -rp "$prompt" num && [[ "$num" ]]; do								#
		[[ "$num" != *[![:digit:]]* ]] && (( num > 0 && num <= ${#file[@]} )) || {				#
			msg="Option invalide : $num"; continue												#
		}																						#
		if [ "$num" == "${#file[@]}" ];then															#
			exit																				#
		fi																						#
		((num--)); msg="${file[num]} a été ${choices[num]:+dé}selectionné"						#
		[[ "${choices[num]}" ]] && choices[num]="" || choices[num]="×"							#
		clear																					#
	done																						#
																								#
	msg="Rien !"																				#
																								#
	for i in "${!file[@]}"; do																	#
		[[ "${choices[i]}" ]] && { tableau[${#tableau[@]}]="${file[i]}"; msg="${tableau[*]}"; }	#
	done																						#
																								#
	echo "Vous avez sélectionné : $msg"															#
}																								#

choix=""	# Initialisation du tableau $choix
machine=""	# Initialisation da la variable $machine
erreur=1	# Initialisation à 1 de la variable $erreur
liste=""	# Initialisation de la liste $liste
pwd=$(pwd)	# Chemin du git, où est le script et les fichiers de configuration

lien=(		# Tuple contenant dans cette ordre le "nom" du programme, le chemin dans "$HOME" des fichiers de configuration et du chemin dans le "$pwd"
	"vim" ".vimrc" "vim/vimrc"                           # Vim
	"i3" ".i3/config" "i3/config"                        # i3
	"i3blocks" ".i3/i3blocks.conf" "i3/i3blocks.conf"    # i3blocks
	"zsh" ".zshrc" "zsh/zshrc"                           # Zsh
	"Xresources" ".Xresources" "Xresources/Xresources"   # Xresources
	"alias" ".aliases" "alias/aliases"                   # Alias
)

portable=("vim" "i3" "i3blocks" "zsh" "Xresources" "alias") # Fichiers de configuration pour le portable
tour=("vim" "i3" "i3blocks" "zsh" "Xresources" "alias")     # Fichiers de configuration pour la tour
serveur=("vim" "zsh" "alias")                               # Fichiers de configuration pour le serveur

configue=(			# Liste des différentes configuration faite
		"Portable"	# Configuration pour le Portable
		"Tour"		# Configuration pour la Tour
		"Serveur"	# Configuation pour le serveur
	"Manuel"	# Choix pour choisir manuelement les fichiers de configuration /!\ Toujours en avant-dernier /!\
	"Quitter"	# Pour quitter le menu /!\ Toujours en dernier /!\
) 					# Liste des différentes configurations

clear	# On efface tout !
while [ "$erreur" -eq 1 ]; do	# Tant qu'il y a des erreurs, on tourne en boucle
	erreur=0				# On dit qu'il y a pas d'erreur
	menu "${configue[@]}"	# Affichage du menu depuis la liste $configue
	case "$choix" in		# Test de la variable $choix
		"${configue[0]}" ) machine=("${portable[@]}");;	# Si la variable $choix correspond à la première valeur de la liste $configue, on duplique la liste $portable dans $machine
		"${configue[1]}" ) machine=("${tour[@]}");;		# Si la variable $choix correspond à la deuxième valeur de la liste $configue, on duplique la liste $tour dans $machine
		"${configue[2]}" ) machine=("${serveur[@]}");;	# Si la variable $choix correspond à la troisième valeur de la liste $configue, on duplique la liste $serveur dans $machine

		"${configue[-2]}" )							# Si la variable $choix correspond à l'avant-dernière valeur de la liste $configue
			clear									# On efface tout !
			for ((i=0; i<"${#lien[@]}"; i+=3)); do	# On parcourt la liste $lien tout les 3 éléments
				liste=("${liste[@]}" "${lien[i]}")	# On duplique tout les trois éléments de la liste $lien dans la liste $liste
			done									# Done !
			liste=("${liste[@]}" "Quitter")			# On rajoute à la fin, dans la liste $liste, "Quitter"
			menu_multiple ${liste[@]}				# On affiche le menu multiple
			machine=("${tableau[@]}")				# On duplique la liste $tableau dans la liste $machine
		;;
		"${configue[-1]}" )	# Si la variable $choix correspond à la dernière valeur de la liste $config
			echo "Quitte"	# On écrit "Quitte"
			exit 0			# Et on quitte le script avec le code 0 (Tout s'est bien fini !)
		;;
		* )						# Pour le reste des correspondance de la variable $choix
		clear					# On efface tout !
		echo "Mauvais choix !"	# On ecrit "Mauvais choix"
		erreur=1				# On met la variable $erreur à 1 !
	esac
	echo ""	# On saut une liste
done

for ((i=0; i<"${#lien[@]}"; i+=3)); do		# On parcourt la liste $lien tout les 3 éléments
	lien_home=$HOME/${lien[i+1]}			# On met le chemin du fichier de configue du $HOME
	lien_git=$pwd/${lien[i+2]}				# On met le chemin du fichier de configue du $pwd

	for index in "${machine[@]}" ; do		# On parcourt la liste $machine

		if [[ "${lien[i]}" == "$index" ]]	# Si l'élément de la liste $lien correspond à $index
		then

			if [ ! -e "$lien_git" ]			# On test si le lien du fichier de configue dans le git existe
			then
				echo "Erreur, le fichier \"$lien_git\" n'existe pas ! ! !"	# Si il existe pas on dit qu'il y a erreur
				exit 1						# Et on quitte avec le code 1 (Erreur dans le script !)
			fi

			echo "Fichiers de configuration pour ${lien[i]}"	# On affiche ça
			if [ -e "$lien_home" ]			# On test si le fichier de configue dans le $HOME existe
			then
				if [ -h "$lien_home" ]		# On test si c'est déjà un lien
				then
					echo "Le fichier est déjà un lien symbolique"	# On affiche
				else
					echo "Le fichier n'est pas un lien"	# Sinon, si c'est pas déjà un lien, on dit ça
					rm -v "$lien_home"					# On supprime le fichier de configue existant
					echo "Création du lien symbolique"	# On dit ça
					ln -sv "$lien_git" "$lien_home"		# On crée le lien symbolique
				fi

			else
				echo "Le fichier n'existe pas"		# Si le fichier n'existe pas, on dit ça
				echo "Création du lien symbolique"	# Aussi ça
				ln -sv "$lien_git" "$lien_home"		# On crée le lien symbolique
			fi
			echo ""	# On saut une ligne
		fi
	done
done
