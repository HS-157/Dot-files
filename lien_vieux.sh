#!/bin/sh
set -u

pwd=$(pwd)
echo "$pwd"

###############
##### Vim #####
###############
echo "Fichier de configuration pour Vim"
vimrc=$HOME/.vimrc
vimrc_git=$pwd/vim/vimrc
if [ -e $vimrc ]
then
	if [ -h $vimrc ]
	then
		echo "Le fichier est déjà un lien symbolique"
	else
		echo "Le fichier n'est pas un lien"
		rm -v $vimrc
		echo "Création du lien symbolique"
		ln -sv $vimrc_git $vimrc
	fi
else
	echo "Le fichier n'existe pas"
	echo "Création du lien symbolique"
	ln -sv $vimrc_git $vimrc
fi
unset vimrc
unset vimrc_git
echo ""

##############
##### i3 #####
##############
echo "Fichier de configuration pour i3"
i3=$HOME/.i3/config
i3_git=$pwd/i3/config
if [ -e $i3 ]
then
	if [ -h $i3 ]
	then
		echo "Le fichier est déjà un lien symbolique"
	else
		echo "Le fichier n'est pas un lien"
		rm -v $i3
		echo "Création du lien symbolique"
		ln -sv $i3_git $i3
	fi
else
	echo "Le fichier n'existe pas"
	echo "Création du lien symbolique"
	ln -sv $i3_git $i3
fi
unset i3
unset i3_git
echo ""

####################
##### i3blocks #####
####################
echo "Fichier de configuration pour i3blocks"
i3blocks=$HOME/.i3/i3blocks.conf
i3blocks_git=$pwd/i3/i3blocks.conf
if [ -e $i3blocks ]
then
	if [ -h $i3blocks ]
	then
		echo "Le fichier est déjà un lien symbolique"
	else
		echo "Le fichier n'est pas un lien"
		rm -v $i3blocks
		echo "Création du lien symbolique"
		ln -sv $i3blocks_git $i3blocks
	fi
else
	echo "Le fichier n'existe pas"
	echo "Création du lien symbolique"
	ln -sv $i3blocks_git $i3blocks
fi
unset i3blocks
unset i3blocks_git
echo ""

###############
##### Zsh #####
###############
echo "Fichier de configuration pour Zsh"
zsh=$HOME/.zshrc
zsh_git=$pwd/zsh/zshrc
if [ -e $zsh ]
then
	if [ -h $zsh ]
	then
		echo "Le fichier est déjà un lien symbolique"
	else
		echo "Le fichier n'est pas un lien"
		rm -v $zsh
		echo "Création du lien symbolique"
		ln -sv $zsh_git $zsh
	fi
else
	echo "Le fichier n'existe pas"
	echo "Création du lien symbolique"
	ln -sv $zsh_git $zsh
fi
unset zsh
unset zsh_git
echo ""

######################
##### Xresources #####
#####################
echo "Fichier de configuration pour Zsh"
Xresources=$HOME/.Xresources
Xresources_git=$pwd/Xresources/Xresources
if [ -e $Xresources ]
then
	if [ -h $Xresources ]
	then
		echo "Le fichier est déjà un lien symbolique"
	else
		echo "Le fichier n'est pas un lien"
		rm -v $Xresources
		echo "Création du lien symbolique"
		ln -sv $Xresources_git $Xresources
	fi
else
	echo "Le fichier n'existe pas"
	echo "Création du lien symbolique"
	ln -sv $Xresources_git $Xresources
fi
unset Xresources
unset Xresources_git
echo ""

#################
##### Alias #####
#################
echo "Fichier des alias"
alias=$HOME/.aliases
alias_git=$pwd/alias/aliases
if [ -e $alias ]
then
	if [ -h $alias ]
	then
		echo "Le fichier est déjà un lien symbolique"
	else
		echo "Le fichier n'est pas un lien"
		rm -v $alias
		echo "Création du lien symbolique"
		ln -sv $alias_git $alias
	fi
else
	echo "Le fichier n'existe pas"
	echo "Création du lien symbolique"
	ln -sv $alias_git $alias
fi
unset alias
unset alias_git
echo ""

